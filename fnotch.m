function [B,A]=fnotch(fs,fd)
% Codigo modificado y extraido de asignatura GIB: Analisis de Se�ales e
% Imagenes Biomedicas
% Funci�n que calcula los coeficientes de un filtro notch
% 
% fd:frecuencia a eliminar;
% fs:frecuencia de muestreo;
% 
%--------------------------------------------------
w = fd/(fs/2)*pi;
B = [1 -2*cos(w) 1];      
A = [1 -2*0.9825*cos(w) 0.9825^2];

dc=1/(1-0.9825+0.9825^2); %ganancia en continua del filtro

B = B/dc;
end
